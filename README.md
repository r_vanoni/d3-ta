# TechnicalChallenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Run project

Run `cd technical-challenge`  
then  
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`

