import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { TaskCardComponent } from './components/task-card/task-card.component';
import { HeaderComponent } from './components/partials/header/header.component';
import { FooterComponent } from './components/partials/footer/footer.component';
import { SumStateHoursPipe } from './pipes/sum-state-hours/sum-state-hours.pipe';

import { DndModule } from 'ngx-drag-drop';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    TaskCardComponent,
    HeaderComponent,
    FooterComponent,
    SumStateHoursPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DndModule,
    FilterPipeModule,
    NgxSmartModalModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
