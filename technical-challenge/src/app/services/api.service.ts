import { Injectable } from '@angular/core';
import { Task } from '../classes/task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  tasks: Task[] = [];

  constructor() { 
    this.tasks.push(new Task(1, "Add forgot password link", "The login form on the website does not have a forgot password link. Please add functionality", 5, "Planned"));
    this.tasks.push(new Task(2, "Bug fixing", "Logout is not working. It gets stuck on loading", 2.5, "In progress"));
    this.tasks.push(new Task(3, "Typo on Main Page", "Please fix typo on the main page: It should say 'If you're looking for...' instead of 'If your looking for..'", 0.5, "Completed"));
  }

  /*
    Note:
      I decided to mock data in this component as if it would be calling an actual API. 
      Normally, most of the logic below would be on the back end instead of this service class. Also every api call would return an Observable containing the response. 
      In this case every call is returning the whole task array because I'm assumming that another user might use the application and change tasks.
      Some workaround had to be done in here so the interaction with the component works flawlessly (i.e. the object assigns and the slices).
  */

  getTasks() : Task[] {
    return this.tasks; 
  }

  createTask(task: Task){ 
    task.Id = this.tasks[this.tasks.length-1].Id+1;
    this.tasks.push(Object.assign({}, task));
    return this.tasks.slice(); //slice was added because Angular was not detecting changes so the total hours calculation wasn't being updated.
  }

  updateTask(task: Task) : Task[] { 
    let index = this.getTaskIndexById(task.Id);
    this.tasks[index] = Object.assign({}, task);
    return this.tasks.slice(); //slice was added because Angular was not detecting changes so the total hours calculation wasn't being updated.
  }

  deleteTask(id: number) { 
    let index = this.getTaskIndexById(id);
    this.tasks.splice(index, 1);
    return this.tasks.slice(); //slice was added because Angular was not detecting changes so the total hours calculation wasn't being updated.
  }

  getTaskIndexById(id: number) { 
    for(let i = 0; i < this.tasks.length; i++){
      if (this.tasks[i].Id == id)
        return i;
    }
    return -1;
  }
}
