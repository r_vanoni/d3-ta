import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/classes/task';

@Component({
  selector: 'task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.css']
})
export class TaskCardComponent implements OnInit {
  @Input() task: Task;
  @Output() onDeleteButtonPressed: EventEmitter<any> = new EventEmitter(); 
  @Output() onEditButtonPressed: EventEmitter<any> = new EventEmitter(); 

  constructor() { }

  ngOnInit() {
  }

  deleteButtonPressed(){
    this.onDeleteButtonPressed.emit(this.task.Id);
  }

  editButtonPressed(){
    this.onEditButtonPressed.emit(this.task.Id);
  }
}
