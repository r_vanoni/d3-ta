import { Component, OnInit } from '@angular/core';
import { DndDropEvent } from 'ngx-drag-drop';
import { Task } from 'src/app/classes/task';
import { ApiService } from 'src/app/services/api.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasks: Task[] = [];
  selectedTask: Task;

  constructor(private apiService: ApiService, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.tasks = this.apiService.getTasks(); //this would actually be an asyncronous call because it'd be calling an API. This is just mocked data for this test.
  }

  onDrop(event: DndDropEvent, newState: string) {
    let task : Task = event.data;
    task.State = newState;
    this.tasks = this.apiService.updateTask(task); //refreshing tasks
  }

  onDeleteButtonPressed(taskId: number) {
    this.selectedTask = this.getTaskById(taskId);
    this.ngxSmartModalService.open("deleteConfirmationModal");    
  }

  onEditButtonPressed(taskId: number) {
    this.selectedTask = Object.assign({}, this.getTaskById(taskId)); //creating new instance so it doesn't modify actual task until saving the changes
    this.ngxSmartModalService.open("saveTaskModal");   
  }

  onNewButtonPressed() {
    this.selectedTask = new Task(null, '', '', null, 'Planned');
    this.ngxSmartModalService.open("saveTaskModal");  
  }

  deleteTask(){
    this.tasks = this.apiService.deleteTask(this.selectedTask.Id);
    this.ngxSmartModalService.closeLatestModal();
  }

  getTaskById(taskId: number) {
    for(let i = 0; i < this.tasks.length; i++){
      if (this.tasks[i].Id == taskId)
        return this.tasks[i];
    }
  }

  saveTask(ngForm: NgForm){
    if (!ngForm.valid)
      return;
    console.log(this.selectedTask.Id);
    if (this.selectedTask.Id) //if id exists, then updates
      this.tasks = this.apiService.updateTask(this.selectedTask);
    else //if there's no id, then it's a new task
      this.tasks = this.apiService.createTask(this.selectedTask);

    this.ngxSmartModalService.closeLatestModal();
    ngForm.reset();
  }
}
