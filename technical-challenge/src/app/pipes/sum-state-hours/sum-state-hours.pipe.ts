import { Pipe, PipeTransform } from '@angular/core';
import { Task } from 'src/app/classes/task';

@Pipe({
  name: 'sumStateHours'
})
export class SumStateHoursPipe implements PipeTransform {

  transform(value: Task[], state?: string): number {
    if (!state)
      return null;
    let totalHs = 0;

    for (let i = 0; i < value.length; i++) {
      const task = value[i];
      if (task.State == state)
        totalHs += task.EstimatedHours;
    }

    return totalHs;
  }

}
