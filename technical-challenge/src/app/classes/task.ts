export class Task {
  Id: number;
  TaskName: string;
  Description: string;
  EstimatedHours: number;
  State: string;

  constructor(id: number, taskname: string, description: string, hours: number, state: string){    
    this.Id = id;
    this.TaskName = taskname;
    this.Description = description;
    this.EstimatedHours = hours;
    this.State = state;
  }
}